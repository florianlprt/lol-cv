import os

from PIL import Image

# script location
script_dir = os.path.dirname(os.path.abspath(__file__))

# load images
background = Image.open(os.path.join(script_dir, '../data/maps/sbire_03.png'))
ashe = Image.open(os.path.join(script_dir, '../data/champions/ashe.png'))

# paste ans show
background.paste(ashe, (100, 100), ashe)
background.show()