import os
import sys
import csv
import numpy as np
import pandas as pd

from PIL import Image, ImageDraw, ImageFilter

# ------------------------------------------------------------------------------
# utils
# ------------------------------------------------------------------------------

N_IMAGES = int(sys.argv[1])
OUT_FOLDER = sys.argv[2]
N_CHAMPIONS = 10
BACKGROUND_SIZE = 256
CHAMPION_SIZE = 24
BBOX_OFFSET = 3
CROP_OFFSET = 16
SHOW_JUNGLE = .25

def mask_circle(image, blur_radius, offset=0):
  offset = blur_radius * 2 + offset
  mask = Image.new("L", image.size, 0)
  draw = ImageDraw.Draw(mask)
  draw.ellipse((offset, offset, image.size[0] - offset, image.size[1] - offset), fill=255)
  mask = mask.filter(ImageFilter.GaussianBlur(blur_radius))
  result = image.copy()
  result.putalpha(mask)
  return result

# ------------------------------------------------------------------------------
# main
# ------------------------------------------------------------------------------

# script location
script_dir = os.path.dirname(os.path.abspath(__file__))

# list images
background_list = os.listdir(os.path.join(script_dir, '../data/maps'))
champion_list = os.listdir(os.path.join(script_dir, '../data/champions'))

# preload common images
jungle_image = Image.open(os.path.join(script_dir, '../data/components/jungle.png'))
fog_image = Image.open(os.path.join(script_dir, '../data/components/map_no_fog.png'))
ring_blue_image = Image.open(os.path.join(script_dir, '../data/components/ring_blue.png'))
ring_red_image = Image.open(os.path.join(script_dir, '../data/components/ring_red.png'))

# resize preloaded images
ring_blue_image = ring_blue_image.resize((CHAMPION_SIZE, CHAMPION_SIZE))
ring_red_image = ring_red_image.resize((CHAMPION_SIZE, CHAMPION_SIZE))

# annotations
annotations = []

# generate maps
for n in range(N_IMAGES):
  # out
  out_file = 'out_{}.jpg'.format(n)

  # select background
  background_file = np.random.choice(background_list)
  background_image = Image.open(os.path.join(script_dir, '../data/maps', background_file))

  # select champions and positions
  champion_files = np.random.choice(champion_list, N_CHAMPIONS, replace=False)
  x_positions = np.random.randint(0, BACKGROUND_SIZE - CHAMPION_SIZE, N_CHAMPIONS)
  y_positions = np.random.randint(0, BACKGROUND_SIZE - CHAMPION_SIZE, N_CHAMPIONS)
  
  # clear fog and paste
  for x_pos, y_pos in zip(x_positions, y_positions):
    fog_image_crop = fog_image.crop((
      x_pos - CROP_OFFSET,
      y_pos - CROP_OFFSET,
      x_pos + CHAMPION_SIZE + CROP_OFFSET,
      y_pos + CHAMPION_SIZE + CROP_OFFSET))
    fog_image_crop = mask_circle(fog_image_crop, 0)
    background_image.paste(fog_image_crop, (x_pos - CROP_OFFSET, y_pos - CROP_OFFSET), fog_image_crop)

  # randomly add jungle and paste
  if np.random.rand() > SHOW_JUNGLE:
    background_image.paste(jungle_image, (0, 0), jungle_image)

  # paste champions and annotate
  for r, (champion_file, x_pos, y_pos) in enumerate(zip(champion_files, x_positions, y_positions)):
    champion_image = Image.open(os.path.join(script_dir, '../data/champions', champion_file))
    champion_image = champion_image.resize((CHAMPION_SIZE, CHAMPION_SIZE))
    ring_image = ring_blue_image if r < 5 else ring_red_image
    background_image.paste(champion_image, (x_pos, y_pos), champion_image)
    background_image.paste(ring_image, (x_pos, y_pos), ring_image)
    annotations.append({
      'image': out_file,
      'xmin': 0 if x_pos - BBOX_OFFSET < 0 else x_pos - BBOX_OFFSET,
      'ymin': 0 if y_pos - BBOX_OFFSET < 0 else y_pos - BBOX_OFFSET,
      'xmax': BACKGROUND_SIZE if x_pos + CHAMPION_SIZE + BBOX_OFFSET > BACKGROUND_SIZE else x_pos + CHAMPION_SIZE + BBOX_OFFSET,
      'ymax': BACKGROUND_SIZE if y_pos + CHAMPION_SIZE + BBOX_OFFSET > BACKGROUND_SIZE else y_pos + CHAMPION_SIZE + BBOX_OFFSET,
      'label': champion_file[:-4]
    })

  # save as jpg
  background_image = background_image.convert('RGB')
  background_image.save(OUT_FOLDER + '/' + out_file, quality=90)

# save annotations
pd.DataFrame(annotations).sort_values('image').to_csv(OUT_FOLDER + '/out_annotations.csv', quoting=csv.QUOTE_NONNUMERIC, index=False)
