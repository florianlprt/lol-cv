import os
import pandas as pd

# script location
script_dir = os.path.dirname(os.path.abspath(__file__))

# list images
champions = os.listdir(os.path.join(script_dir, '../champions'))

# rename champions
for c in champions:
  pre, tail = c.split('_')
  num, last = tail.split(' ')
  if len(num) == 2:
    num = '0' + num
  filename = pre + '_' + num + '.png'
  os.rename(
    os.path.join(script_dir, '../champions/', c),
    os.path.join(script_dir, '../champions/', filename))


