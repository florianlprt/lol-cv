import os
import pandas as pd

# script location
script_dir = os.path.dirname(os.path.abspath(__file__))

# list images
champions = os.listdir(script_dir)

# csv
csv = []
for c in champions:
  csv.append({ 'champion': script_dir + '\\' + c })

pd.DataFrame(csv).sort_values('champion').to_csv('variables.txt', index=False)