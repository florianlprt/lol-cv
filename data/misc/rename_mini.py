import os
import pandas as pd

# script location
script_dir = os.path.dirname(os.path.abspath(__file__))

# list images
images = os.listdir(os.path.join(script_dir, '../champions'))
images = sorted(images)

# list champions
champions = os.listdir('C:\\Users\\Flo\\Downloads\\ddragon\\10.19.1\\img\\champion')
champions = sorted(champions)

# rename images
for i, c in zip(images, champions):
  os.rename(
    os.path.join(script_dir, '../champions/', i),
    os.path.join(script_dir, '../champions/', c))


